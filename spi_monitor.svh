`ifndef SPI_MONITOR_SVH
`define SPI_MONITOR_SVH
class spi_monitor extends uvm_monitor;

	`uvm_component_utils(spi_monitor)

	uvm_analysis_port#(spi_transaction) ap;
	virtual spi_if my_spi_if;

	function new(string name, uvm_component parent);
		super.new(name, parent);
		ap = new(.name("ap"), .parent(this));
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		if(!uvm_config_db#(virtual spi_if)::get(.cntxt(this), .inst_name(""),.field_name("spi_if"),.value(my_spi_if))) begin
			`uvm_fatal(get_name(), "SPI Monitor cannot find spi_if")
		end
	endfunction: build_phase

	task run_phase(uvm_phase phase);
		spi_transaction tx/* = spi_transaction::type_id::create("tx",.contxt(get_full_name()))*/;

		forever begin
			tx = spi_transaction::type_id::create("tx");
			phase.raise_objection(.obj(this), .description(get_name()));

			fork
				convert_stimulus_to_transaction(tx);
				wait_for_interrupt(tx);
			join


			phase.drop_objection(.obj(this), .description(get_name()));
		end
	endtask: run_phase

	task convert_stimulus_to_transaction(spi_transaction tx);
		forever begin
			if(0 == my_spi_if.irq || 1'bx === my_spi_if.irq) begin
				//uvm_report_info(get_name(), ": tx.queue.push_back()");
				@my_spi_if.cb;
				tx.queue.push_back(my_spi_if.mosi);
				//$display("tx.queue.size(): %0d", tx.queue.size());
				/*
				if(128 == tx.queue.size()) begin // FIXME: ad hoc solution for 128 char only
					break;
				end
				*/
			end
		end
	endtask: convert_stimulus_to_transaction

	task wait_for_interrupt(spi_transaction tx);
		forever begin
			@(posedge my_spi_if.irq);
			ap.write(tx);
			uvm_report_info(get_name(), ": spi_monitor write out to spi_sequencer and scoreboard");
			// TODO: clear the queue (either somewhere here or in the comparator
		end
	endtask: wait_for_interrupt

endclass: spi_monitor
`endif
