`timescale 1ns/1ps

import apb_pkg::*;
import uvm_pkg::*;

interface apb_if( input logic clk );
	logic		presetn;
	logic	[4:0]	paddr;
	logic		psel;
	logic		penable;
	logic		pwrite;
	logic	[31:0]	prdata;
	logic	[31:0]	pwdata;
	logic		pready;
	logic		irq;

	clocking cb @(posedge clk);
		output	presetn, paddr, psel, penable, pwrite, pwdata;
		input	prdata, pready, irq;
	endclocking: cb
    
	modport master(input clk, prdata, pready, irq, output presetn, paddr, psel, penable, pwrite, pwdata);
	modport slave(input clk, presetn, paddr, psel, penable, pwrite, pwdata, output prdata, pready, irq);

	property APB_ENABLE_AFTER_PSEL;
		@(posedge clk)
			presetn & psel & !penable |-> ##1 penable;
	endproperty: APB_ENABLE_AFTER_PSEL
	apb_enable_after_psel: assert property (APB_ENABLE_AFTER_PSEL);

	property APB_DEASSERT_ENABLE_AFTER_READY;
		@(posedge clk)
			presetn & penable & pready |-> ##1 !penable;
	endproperty: APB_DEASSERT_ENABLE_AFTER_READY
	apb_deassert_enable_after_ready: assert property (APB_DEASSERT_ENABLE_AFTER_READY);

	covergroup cg_apb @(posedge clk);
		option.per_instance = 1;
		coverpoint paddr	{	bins data0_reg	= {8'h00};
						bins data1_reg	= {8'h04};
						bins data2_reg	= {8'h08};
						bins data3_reg	= {8'h0c};
						bins ctrl_reg	= {8'h10};
						bins div_reg	= {8'h14};
						bins ss_reg	= {8'h18};
					}
	endgroup: cg_apb

	cg_apb m_cg_apb = new();

endinterface: apb_if

interface spi_if();
	logic	sclk;
	logic	mosi;
	logic	miso;
	logic	ss;
	logic	irq;

	event interrupt;

	clocking cb @(posedge sclk);
		output	miso;
		input	mosi, ss/*, irq*/;
	endclocking: cb

	modport master(input miso, output sclk, mosi, ss, irq);
	modport slave(input sclk, mosi, ss, irq, output miso);

	task check_for_interrupt;
		@(posedge irq);
			-> interrupt;
	endtask: check_for_interrupt

endinterface: spi_if

module top;

	logic clk=1;
	wire irq;

	initial begin: clk_gen
		forever #5 clk = ~clk;
	end: clk_gen

	// Instantiate interfaces
	apb_if my_apb_if(.clk(clk));
	spi_if my_spi_if();

	assign my_spi_if.irq = irq;
	assign my_apb_if.irq = irq;

	// Instantiate DUT
	spi_top DUT	(	.PCLK(my_apb_if.slave.clk),
				.PRESETN(my_apb_if.slave.presetn),
				.PADDR(my_apb_if.slave.paddr),
				.PWDATA(my_apb_if.slave.pwdata),
				.PRDATA(my_apb_if.slave.prdata),
				.PWRITE(my_apb_if.slave.pwrite),
				.PSEL(my_apb_if.slave.psel),
				.PENABLE(my_apb_if.slave.penable),
				.PREADY(my_apb_if.slave.pready),
				.IRQ(irq),

				.ss_pad_o(my_spi_if.master.ss),
				.sclk_pad_o(my_spi_if.master.sclk),
				.mosi_pad_o(my_spi_if.master.mosi),
				.miso_pad_i(my_spi_if.master.miso)
			 );
                                
	initial begin: start_test
		uvm_config_db#(virtual spi_if)::set(.cntxt(null), .inst_name("uvm_test_top.*"), .field_name("spi_if"), .value(my_spi_if));
		uvm_config_db#(virtual apb_if)::set(.cntxt(null), .inst_name("uvm_test_top.*"), .field_name("vif"), .value(my_apb_if));
		run_test();
	end: start_test
endmodule: top
