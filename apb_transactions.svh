`ifndef APB_TRANSACTIONS_SVH
`define APB_TRANSACTIONS_SVH
class apb_transaction extends uvm_sequence_item;

	`uvm_object_utils(apb_transaction)

	bit			presetn = 1'b1;
	bit			irq;
	int unsigned		prdata;
	int unsigned		pready;
	rand bit [4:0]		paddr;
	rand int unsigned	pwdata;
	rand bit		penable;
	rand bit		psel;
	rand bit		pwrite;

	function new(string name="apb_transaction");
		super.new(name);
	endfunction: new

	function string convert2string();
		string s = super.convert2string();
		s = {s, $sformatf("\n paddr: %0h, pwdata: %0h, prdata: %0h", paddr, pwdata, prdata)};
		s = {s, $sformatf("\n penable: %0b, psel: %0b, pwrite: %0b", penable, psel, pwrite)};
		return s;
	endfunction: convert2string

endclass: apb_transaction
`endif
