`ifndef SPI_SEQUENCES_SVH
`define SPI_SEQUENCES_SVH

class spi_sequence extends uvm_sequence#(spi_transaction);

	`uvm_object_utils(spi_sequence)
	`uvm_declare_p_sequencer(spi_sequencer)

	spi_transaction tx; // Create transaction handle

	function new(string name="");
		super.new(name);
	endfunction: new

	task body();
		
		forever begin
			p_sequencer.tlm_fifo.get(tx);
			start_item(tx);
			//assert(tx.randomize());
			finish_item(tx);
		end
	endtask: body

endclass: spi_sequence

`endif
