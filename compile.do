onerror { quit }

vlib apb_uvm

vlog \
-work apb_uvm \
+incdir+"$aldec/vlib/uvm-1.2/src"+hdl \
-l uvm_1_2 \
-coverage sbeca \
-error_limit 1 \
-msg 1 \
hdl/timescale.v     \
hdl/spi_defines.v   \
hdl/spi_clgen.v     \
hdl/spi_shift.v     \
hdl/spi_top.v       \
apb_pkg.sv          \
top.sv

#do sim.do
