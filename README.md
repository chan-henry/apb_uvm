# apb_uvm

This repo holds an ongoing Advanced Peripheral Bus (APB) UVM testbench project.

Compilation and simulation is performed in Aldec's Riviera-PRO.

Block Diagram:

![alt text](apb_uvm.png?raw=true "APB_UVM TB Block Diagram")

![alt text](APB_transactions.png?raw=true "APB transactions waveform")

![alt text](SPI_response.png?raw=true "SPI reactive driver response")
