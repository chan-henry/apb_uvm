`ifndef SPI_TRANSACTIONS_SVH
`define SPI_TRANSACTIONS_SVH
class spi_transaction extends uvm_sequence_item;

	`uvm_object_utils(spi_transaction)

	bit sclk;
	bit ss;
	bit mosi;
	rand bit miso;
	rand bit [127:0] data;
	bit queue[$];

	int unsigned size;
	bit tmp;

	function new(string name="");
		super.new(name);
	endfunction: new

	function string convert2string();
		string s/* = super.convert2string()*/;

		size = queue.size();
		while(0 != queue.size()) begin
			tmp = queue.pop_front();
			//$display("tmp: %0b", tmp);
			s = {s, $sformatf("%0b", tmp)};
		end

		s = $sformatf("\n spi_data: %0d'b%s", size, s);
		//$display("s = %0s", s);
		return s;
	endfunction: convert2string

endclass: spi_transaction
`endif
