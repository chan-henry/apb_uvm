`ifndef SPI_AGENT_SVH
`define SPI_AGENT_SVH
class spi_agent extends uvm_agent; 

	`uvm_component_utils(spi_agent)

	uvm_analysis_port#(spi_transaction) ap;

	spi_driver	spi_driver_inst;
	spi_sequencer	spi_sequencer_inst;
	spi_monitor	spi_monitor_inst;

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		spi_driver_inst		= spi_driver::type_id::create("spi_driver_inst", this);
		spi_sequencer_inst	= spi_sequencer::type_id::create("spi_sequencer_inst", this);
		spi_monitor_inst	= spi_monitor::type_id::create("spi_monitor_inst", this);

		ap = new(.name("ap"),.parent(this));
	endfunction: build_phase

	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);

		spi_driver_inst.seq_item_port.connect(spi_sequencer_inst.seq_item_export);
		spi_monitor_inst.ap.connect(spi_sequencer_inst.exprt);
		spi_monitor_inst.ap.connect(ap);
		uvm_report_info(get_name(), ": connect_phase, driver, monitor, and sequencer connected");
	endfunction: connect_phase

endclass: spi_agent
`endif
