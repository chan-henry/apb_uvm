`ifndef APB_TEST_SVH
`define APB_TEST_SVH
class apb_test extends uvm_test;

	`uvm_component_utils(apb_test)

	apb_env		apb_env_inst;

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		apb_env_inst	= apb_env::type_id::create(.name("apb_env_inst"), .parent(this));
	endfunction: build_phase

	task run_phase(uvm_phase phase);
		// Start sequences here
		apb_reset			my_apb_reset;
		apb_seq_write_0xdeadbeef	my_apb_seq_write_0xdeadbeef;
		apb_seq_read_0xdeadbeef		my_apb_seq_read_0xdeadbeef;
		apb_seq_set_csr			my_apb_seq_set_csr;
		apb_sequence_library		my_apb_seq_lib;
		spi_sequence			my_spi_seq;

		phase.raise_objection(.obj(this),.description(get_name()));

		my_apb_reset = apb_reset::type_id::create("my_apb_reset");
		my_apb_seq_write_0xdeadbeef = apb_seq_write_0xdeadbeef::type_id::create("my_apb_seq_write_0xdeadbeef");
		my_apb_seq_read_0xdeadbeef = apb_seq_read_0xdeadbeef::type_id::create("my_apb_seq_read_0xdeadbeef");
		my_apb_seq_set_csr = apb_seq_set_csr::type_id::create("my_apb_seq_set_csr");
		my_apb_seq_lib = apb_sequence_library::type_id::create("my_apb_seq_lib");
		my_spi_seq = spi_sequence::type_id::create("my_spi_seq");

		my_apb_seq_lib.min_random_count=90000;
		my_apb_seq_lib.max_random_count=100000;
		
		if(!my_apb_seq_lib.randomize()) begin
		    `uvm_error(get_name(), "The my_apb_seq_lib failed to randomize")
		end
		
		`uvm_info("my_apb_seq_lib", my_apb_seq_lib.convert2string(), UVM_NONE)

		//fork
			my_apb_reset.start(apb_env_inst.apb_agent_inst.apb_sequencer_inst);
			my_apb_seq_write_0xdeadbeef.start(apb_env_inst.apb_agent_inst.apb_sequencer_inst);
		fork
			my_apb_seq_set_csr.start(apb_env_inst.apb_agent_inst.apb_sequencer_inst);
			my_spi_seq.start(apb_env_inst.spi_agent_inst.spi_sequencer_inst);
		join_any
			//my_apb_seq_read_0xdeadbeef.start(apb_env_inst.apb_agent_inst.apb_sequencer_inst);
			//my_apb_seq_set_csr.start(apb_env_inst.apb_agent_inst.apb_sequencer_inst);
			my_apb_seq_lib.start(apb_env_inst.apb_agent_inst.apb_sequencer_inst);
		//join_none
		
		//#20000ns	// Wait for DUT to transmit SPI signals after receiving the APB stimulus
		phase.phase_done.set_drain_time(this, 8000000ns);	// Is there a more elegant solution to ending the sim?
		
		phase.drop_objection(this);
	endtask: run_phase

endclass: apb_test
`endif
