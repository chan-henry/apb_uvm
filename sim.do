#####################################
# Interesting seed values:
# - 10027
#####################################

vsim \
-sv_seed random \
+UVM_TESTNAME=apb_test \
+access +r \
-acdb \
apb_uvm.top

log -rec *

run -all

endsim

do cov_report.do

#acdb report \
#-html \
#-i apb_uvm.acdb \
#-o coverage_report.html

quit
