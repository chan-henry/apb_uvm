`ifndef SPI_SEQUENCER_SVH
`define SPI_SEQUENCER_SVH
class spi_sequencer extends uvm_sequencer#(spi_transaction);

  `uvm_component_utils(spi_sequencer)
  
  uvm_analysis_export#(spi_transaction) exprt;
  uvm_tlm_analysis_fifo#(spi_transaction) tlm_fifo;
  
  function new(string name, uvm_component parent);
    super.new(name, parent);
    
    exprt = new("exprt", this);
    tlm_fifo = new("tlm_fifo", this);
  endfunction: new
  
  function void connect_phase(uvm_phase phase);
    super.connect_phase(phase);
    
    exprt.connect(tlm_fifo.analysis_export);
  endfunction: connect_phase
  
endclass: spi_sequencer
`endif