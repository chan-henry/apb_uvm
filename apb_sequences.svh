`ifndef APB_SEQUENCES_SVH
`define APB_SEQUENCES_SVH

class apb_sequence extends uvm_sequence#(apb_transaction);

	`uvm_object_utils(apb_sequence)

	int unsigned count = 5000; // Knob for # of transactions

	function new(string name="");
		super.new(name);
	endfunction: new

	task body();
		apb_transaction tx; // Create transaction handle

		repeat(count) begin: tx_create_loop // Does this create a memory leak?
			tx = apb_transaction::type_id::create("tx", .contxt(get_full_name()));
			start_item(tx);
			tx.randomize();
			finish_item(tx);
		end: tx_create_loop
	endtask: body

endclass: apb_sequence

class apb_reset extends uvm_sequence#(apb_transaction);

	`uvm_object_utils(apb_reset)

	function new(string name="apb_reset");
		super.new(name);
	endfunction: new

	task body();
		apb_transaction tx; // Create transaction handle

		tx = apb_transaction::type_id::create("tx", .contxt(get_full_name()));
		start_item(tx);
		tx.presetn = 0;
		tx.paddr = 0;
		tx.pwrite = 0;
		tx.psel = 0;
		tx.pwdata = 0;
		tx.penable = 0;
		finish_item(tx);
	endtask: body

endclass: apb_reset

class apb_write extends uvm_sequence#(apb_transaction);

	`uvm_object_utils(apb_write)

	randc bit	[4:0]	addr;
	randc bit	[31:0]	data;

	// Byte aligned, limit addr to the 7 regs
	constraint addr_range	{	addr[4:2] inside {[0:6]};
					addr[1:0] == 0;
				}

	constraint always_ie	{	if(5'h10 == addr)
						data[12] == 1'b1;
				}

	function new(string name="apb_write");
		super.new(name);
	endfunction: new

		/**********************************************
			 T1  T2  T3  T4
			  _   _   _   _
		PCLK	_| |_| |_| |_| |_
			__ ________
		PADDR	__X__addr__X
			   ________
		PWRITE	__/        \
			   ________
		PSEL	__/        \
			__ ________
		PWDATA	__X__data__X
			       ____
		PENABLE	______/    \
			       ____
		PREADY	______/    \
		
		**********************************************/

	task body();
		apb_transaction tx; // Create transaction handle
		tx = apb_transaction::type_id::create("tx", .contxt(get_full_name()));

		wait_for_grant();
		tx.randomize();
		tx.paddr = addr;
		tx.pwdata = data;
		tx.pwrite = 1;
		tx.psel = 1;
		tx.penable = 1;
		send_request(tx);
		wait_for_item_done();

	endtask: body

endclass: apb_write

class apb_read extends uvm_sequence#(apb_transaction);

	`uvm_object_utils(apb_read)

	randc bit	[4:0]	addr;

	// Byte aligned, limit addr to the 7 regs
	constraint addr_range	{	addr[4:2] inside {[0:6]}; 
					addr[1:0] == 0;
				}

	function new(string name="apb_read");
		super.new(name);
	endfunction: new

	task body();
		apb_transaction tx; // Create transaction handle

		tx = apb_transaction::type_id::create("tx", .contxt(get_full_name()));

		wait_for_grant();
		tx.randomize();
		tx.paddr = addr;
		tx.pwrite = 0;
		tx.psel = 1;
		tx.penable = 1;
		send_request(tx);
		wait_for_item_done();
	endtask: body

endclass: apb_read
/*
class apb_idle extends uvm_sequence#(apb_transaction);

	`uvm_object_utils(apb_idle)

	function new(string name="apb_idle");
		super.new(name);
	endfunction: new

	task body();
		apb_transaction tx;
		tx = apb_transaction::type_id::create("tx", .contxt(get_full_name()));

		wait_for_grant();
		assert(tx.randomize());
		tx.psel = 0;
		tx.penable = 0;
		send_request(tx);
		wait_for_item_done();
	endtask: body

endclass: apb_idle
*/
class apb_sequence_library extends uvm_sequence_library#(apb_transaction);

	`uvm_object_utils(apb_sequence_library)

	function new(string name="apb_sequence_library");
		super.new(name);
        
        add_typewide_sequences({apb_write::get_type(),apb_read::get_type()});
        init_sequence_library();
	endfunction: new

endclass: apb_sequence_library


/***********************************************




***********************************************/

class apb_seq_write_0xdeadbeef extends uvm_sequence#(apb_transaction);

	`uvm_object_utils(apb_seq_write_0xdeadbeef)

	function new(string name="apb_seq_write_0xdeadbeef");
		super.new(name);
	endfunction: new

	task body();
		apb_transaction tx; // Create transaction handle
		tx = apb_transaction::type_id::create("tx", .contxt(get_full_name()));

		wait_for_grant();
		tx.paddr = 5'h00; //Tx0
		tx.pwdata = 32'hdeadbeef;
		tx.pwrite = 1;
		tx.psel = 1;
		tx.penable = 1;
		send_request(tx);
		wait_for_item_done();
		
		wait_for_grant();
		tx.paddr = 5'h04; //Tx1
		tx.pwdata = 32'hbadb100d;
		tx.pwrite = 1;
		tx.psel = 1;
		tx.penable = 1;
		send_request(tx);
		wait_for_item_done();
		
		wait_for_grant();
		tx.paddr = 5'h08; //Tx2
		tx.pwdata = 32'hbaaaaaad;
		tx.pwrite = 1;
		tx.psel = 1;
		tx.penable = 1;
		send_request(tx);
		wait_for_item_done();
		
		wait_for_grant();
		tx.paddr = 5'h0C; //Tx3
		tx.pwdata = 32'hba5eba11;
		tx.pwrite = 1;
		tx.psel = 1;
		tx.penable = 1;
		send_request(tx);
		wait_for_item_done();
	endtask: body

endclass: apb_seq_write_0xdeadbeef

class apb_seq_read_0xdeadbeef extends uvm_sequence#(apb_transaction);

	`uvm_object_utils(apb_seq_read_0xdeadbeef)

	function new(string name="apb_seq_read_0xdeadbeef");
		super.new(name);
	endfunction: new

	task body();
		apb_transaction tx; // Create transaction handle
		tx = apb_transaction::type_id::create("tx", .contxt(get_full_name()));

		wait_for_grant();
		tx.paddr = 5'h00; //Rx0
		tx.pwrite = 0;
		tx.psel = 1;
		tx.penable = 1;
		send_request(tx);
		wait_for_item_done();
		
		wait_for_grant();
		tx.paddr = 5'h04; //Rx1
		tx.pwrite = 0;
		tx.psel = 1;
		tx.penable = 1;
		send_request(tx);
		wait_for_item_done();
		
		wait_for_grant();
		tx.paddr = 5'h08; //Rx2
		tx.pwrite = 0;
		tx.psel = 1;
		tx.penable = 1;
		send_request(tx);
		wait_for_item_done();
		
		wait_for_grant();
		tx.paddr = 5'h0C; //Rx3
		tx.pwrite = 0;
		tx.psel = 1;
		tx.penable = 1;
		send_request(tx);
		wait_for_item_done();
	endtask: body

endclass: apb_seq_read_0xdeadbeef

class apb_seq_set_csr extends uvm_sequence#(apb_transaction);

	`uvm_object_utils(apb_seq_set_csr)

	function new(string name="apb_seq_set_csr");
		super.new(name);
	endfunction: new

	task body();
		apb_transaction tx;
		tx = apb_transaction::type_id::create("tx", .contxt(get_full_name()));
		// Should there be two transactions here, (tx_data, tx_csr) where tx_data
		// is sent first, then tx_csr? Or keep it as is where tx is just updated and sent again?
		
		wait_for_grant();
		/* Can these assignments be moved before "wait_for_grant()" to save time?
		1: Automatic Slave Select
		1: Interrupt Enable
		0: LSB
		0: TX_NEG
		0: RX_NEG
		0: GO_BSY
		0x00: CHAR_LEN (128 bit transfer)
		*/
		tx.paddr = 5'h10;
		tx.pwdata = 32'h3000;
		tx.pwrite = 1;
		tx.psel = 1;
		tx.penable = 1;
		send_request(tx);
		wait_for_item_done();
		
		wait_for_grant();
		tx.pwdata = 32'h3100; // set GO_BSY
		send_request(tx);
		wait_for_item_done();

	endtask: body

endclass: apb_seq_set_csr

class apb_seq_go_bsy extends uvm_sequence#(apb_transaction);

	`uvm_object_utils(apb_seq_go_bsy)

	function new(string name="apb_seq_go_bsy");
		super.new(name);
	endfunction: new

	task body();
		apb_transaction tx; // Create transaction handle
		tx = apb_transaction::type_id::create("tx", .contxt(get_full_name()));
		
		wait_for_grant();
		tx.paddr = 5'h10;
		tx.pwdata[8] = 1'b1; // set GO_BSY
		tx.pwrite = 1;
		tx.psel = 1;
		tx.penable = 1;
		send_request(tx);
		wait_for_item_done();

	endtask: body

endclass: apb_seq_go_bsy

// TODO: Write general sequences which write/read to the DUT registers
// TODO: apb_seq_write_tx0
// TODO: apb_seq_write_tx1
// TODO: apb_seq_write_tx2
// TODO: apb_seq_write_tx3
// TODO: apb_seq_write_csr
// TODO: apb_seq_write_div
// TODO: apb_seq_write_ss


`endif
