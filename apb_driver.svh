`ifndef APB_DRIVER_SVH
`define APB_DRIVER_SVH
class apb_driver extends uvm_driver#(apb_transaction);
 
	`uvm_component_utils(apb_driver)

	virtual apb_if my_apb_if;

	function new (string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		if(!uvm_config_db#(virtual apb_if)::get(.cntxt(this), .inst_name(""), .field_name("vif"), .value(my_apb_if))) begin
			`uvm_fatal(get_name(), "Failed to get virtual interface for driver")
		end
	endfunction: build_phase

	task run_phase(uvm_phase phase);
		apb_transaction tx;

		forever begin: driver_run_phase
			//seq_item_port.get_next_item(tx);
			seq_item_port.try_next_item(tx);
			phase.raise_objection(.obj(this), .description(get_name()));
	
			/**********************************************
				 T1  T2  T3  T4
				  _   _   _   _
			PCLK	_| |_| |_| |_| |_
				__ ________
			PADDR	__X__addr__X
				   ________
			PWRITE	__/        \
				   ________
			PSEL	__/        \
				__ ________
			PWDATA	__X__data__X
				       ____
			PENABLE	______/    \
				       ____
			PREADY	______/    \
			
			**********************************************/

			while( null == tx ) begin: idle
				@my_apb_if.cb;
				my_apb_if.cb.presetn	<= '1;
				//my_apb_if.cb.paddr	<= '0;
				//my_apb_if.cb.pwrite	<= '0;
				my_apb_if.cb.psel	<= '0;
				//my_apb_if.cb.pwdata	<= '0;
				my_apb_if.cb.penable	<= '0;
			end: idle

			@my_apb_if.cb;	// Setup
				my_apb_if.cb.presetn	<= tx.presetn;
				my_apb_if.cb.paddr	<= tx.paddr;
				my_apb_if.cb.pwrite	<= tx.pwrite;
				my_apb_if.cb.psel	<= tx.psel;
				my_apb_if.cb.pwdata	<= tx.pwdata;
				my_apb_if.cb.penable	<= 1'b0;
			@my_apb_if.cb;	// Access
				my_apb_if.cb.penable	<= tx.penable;
			//@my_apb_if.cb;
				if(tx.presetn == 1 || tx.psel == 1) begin
				// if not in a reset/idle state, wait for the slave to receive the transmission
					while(my_apb_if.master.pready == 0) begin
						@my_apb_if.cb;
					end
					//my_apb_if.cb.penable <= 1'b0;
				end

			seq_item_port.item_done();
			phase.drop_objection(.obj(this), .description(get_name()));
		end: driver_run_phase
	endtask: run_phase

endclass: apb_driver
`endif
