`ifndef APB_MONITOR_SVH
`define APB_MONITOR_SVH
class apb_monitor extends uvm_monitor;

	`uvm_component_utils(apb_monitor)

	uvm_analysis_port#(apb_transaction) ap;
	virtual apb_if my_apb_if;
	local bit tip = 0; // SPI Transmission in progress

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		if(!uvm_config_db#(virtual apb_if)::get(.cntxt(this),.inst_name(""),.field_name("vif"),.value(my_apb_if))) begin
			`uvm_fatal(get_name(), "Monitor cannot find apb_if")
		end

		ap = new(.name("ap"), .parent(this));
	endfunction: build_phase

	task run_phase(uvm_phase phase);
		apb_transaction tx = apb_transaction::type_id::create("tx");

		forever begin
			phase.raise_objection(.obj(this),.description(get_name()));

			fork
				convert_stimulus_to_transaction(tx);
				wait_for_interrupt(tx);
			join


			phase.drop_objection(.obj(this),.description(get_name()));
		end

	endtask: run_phase

	task convert_stimulus_to_transaction(apb_transaction tx);
		forever begin
			@my_apb_if.cb;
			tx.presetn	=	my_apb_if.master.presetn;
			tx.paddr	=	my_apb_if.master.paddr;
			tx.pwdata	=	my_apb_if.master.pwdata;
			tx.prdata	=	my_apb_if.master.prdata;
			tx.psel		=	my_apb_if.master.psel;
			tx.penable	=	my_apb_if.master.penable;
			tx.pwrite	=	my_apb_if.master.pwrite;
			tx.irq		=	my_apb_if.master.irq;

			if(1'b1 == tx.presetn && 1'b1 == tx.penable && 1'b1 == tx.psel) begin
				if(5'h10 == tx.paddr && 1'b1 == tx.pwrite && 1'b1 == tx.pwdata[8]) begin
					if(1'b0 == tip) begin
						tip = 1'b1;
						ap.write(tx);
					end
				end
				else if(5'h0C >= tx.paddr && 1'b1 == tx.pwrite) begin
					ap.write(tx);
				end
			end
		end
	endtask: convert_stimulus_to_transaction

	task wait_for_interrupt(apb_transaction tx);
		forever begin
			@(posedge my_apb_if.irq);
				tip = 1'b0;
		end
	endtask: wait_for_interrupt
endclass: apb_monitor
`endif
