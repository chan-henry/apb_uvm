package apb_pkg;

  import uvm_pkg::*;
  `include "uvm_macros.svh"

//  `include "ral/reg_model.svh"

  `include "spi_transactions.svh"
  `include "spi_sequencer.svh"
  `include "spi_sequences.svh"
  `include "spi_monitor.svh"
  `include "spi_driver.svh"

  `include "apb_transactions.svh"
  typedef uvm_sequencer#(apb_transaction) apb_sequencer;
  `include "apb_scoreboard.svh"
  `include "apb_sequences.svh"
  `include "apb_monitor.svh"
  `include "apb_driver.svh"

  `include "spi_agent.svh"
  `include "apb_agent.svh"
  `include "apb_env.svh"
  `include "apb_test.svh"

endpackage: apb_pkg
