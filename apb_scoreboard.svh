`ifndef APB_SCOREBOARD_SVH
`define APB_SCOREBOARD_SVH

class predictor extends uvm_subscriber#(apb_transaction);

	`uvm_component_utils(predictor)

	uvm_analysis_port#(spi_transaction) results_ap;

	//bit		queue[$];
	//bit [127:0]	data = 'd0;
	bit [6:0]	char_len = 'd0;
	bit [31:0]	reg_file[bit[4:0]] = 	'{	5'h00:'d0,		// DATA0
							5'h04:'d0,		// DATA1
							5'h08:'d0,		// DATA2
							5'h0C:'d0,		// DATA3
							5'h10:'d0,		// CTRL
							5'h14:32'h0000_FFFF,	// CLKDIV
							5'h18:'d0		// SLAVE_SELECT
						};

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		results_ap = new("results_ap", this);
	endfunction: build_phase

	function void write(apb_transaction t);
		spi_transaction exp_tx = spi_transaction::type_id::create("exp_tx");

		// TODO: is clearing the queue necessary if I'm creating a new exp_tx on each write call?
		while(0 != exp_tx.queue.size()) begin	// EMPTY THE QUEUE
			exp_tx.queue.pop_back();
		end

		// TODO: RESET

		if(1'b1 == t.presetn && 1'b1 == t.penable && 1'b1 == t.psel)  begin // Non-reset, Write/Read
			// WRITE DATA_REG
			if(5'h0C >= t.paddr && 1'b1 == t.pwrite) begin
				reg_file[t.paddr] = t.pwdata;
			end

			// WRITE CTRL
			// TODO: DUT does not perform to spec: char_len can be changed in same cycle when GO_BSY is set
			/*
			if(5'h10 == t.paddr && 1'b1 == t.pwrite && 1'b0 == t.pwdata[8]) begin
				char_len = t.pwdata[6:0];
				reg_file[5'h10] = t.pwdata;
			end
			*/
			// WRITE CTRL GO_BSY
			if(5'h10 == t.paddr && 1'b1 == t.pwrite && 1'b1 == t.pwdata[8]) begin
				char_len = t.pwdata[6:0];
				//$display("char_len: %0d", char_len);
				reg_file[5'h10] = t.pwdata;
				//data = {reg_file[5'h00], reg_file[5'h04], reg_file[5'h08], reg_file[5'h0C]};

				if(0 == char_len) begin	// 128 Characters
					for(int i=31; i >= 0; i--) begin
						exp_tx.queue.push_back(reg_file[5'h0C][i]);
						reg_file[5'h0C][i] = 1'b0;
					end
					
					for(int i=31; i >= 0; i--) begin
						exp_tx.queue.push_back(reg_file[5'h08][i]);
						reg_file[5'h08][i] = 1'b0;
					end

					for(int i=31; i >= 0; i--) begin
						exp_tx.queue.push_back(reg_file[5'h04][i]);
						reg_file[5'h04][i] = 1'b0;
					end

					for(int i=31; i >= 0; i--) begin
						exp_tx.queue.push_back(reg_file[5'h00][i]);
						reg_file[5'h00][i] = 1'b0;
					end
					results_ap.write(exp_tx);
				end

				else if(127 >= char_len >= 97) begin // 127-97 Characters
					for(int i= char_len-97; i >= 0; i--) begin
						exp_tx.queue.push_back(reg_file[5'h0C][i]);
						reg_file[5'h0C][i] = 1'b0;
					end
					
					for(int i=31; i >= 0; i--) begin
						exp_tx.queue.push_back(reg_file[5'h08][i]);
						reg_file[5'h08][i] = 1'b0;
					end

					for(int i=31; i >= 0; i--) begin
						exp_tx.queue.push_back(reg_file[5'h04][i]);
						reg_file[5'h04][i] = 1'b0;
					end

					for(int i=31; i >= 0; i--) begin
						exp_tx.queue.push_back(reg_file[5'h00][i]);
						reg_file[5'h00][i] = 1'b0;
					end
					results_ap.write(exp_tx);
				end

				else if(96 >= char_len >= 65) begin // 96-65 Characters
					for(int i=char_len-65; i >= 0; i--) begin
						exp_tx.queue.push_back(reg_file[5'h08][i]);
						reg_file[5'h08][i] = 1'b0;
					end

					for(int i=31; i >= 0; i--) begin
						exp_tx.queue.push_back(reg_file[5'h04][i]);
						reg_file[5'h04][i] = 1'b0;
					end

					for(int i=31; i >= 0; i--) begin
						exp_tx.queue.push_back(reg_file[5'h00][i]);
						reg_file[5'h00][i] = 1'b0;
					end
					results_ap.write(exp_tx);
				end

				else if(64 >= char_len >= 33) begin // 64-33 Characters
					for(int i= char_len-33; i >= 0; i--) begin
						exp_tx.queue.push_back(reg_file[5'h04][i]);
						reg_file[5'h04][i] = 1'b0;
					end

					for(int i=31; i >= 0; i--) begin
						exp_tx.queue.push_back(reg_file[5'h00][i]);
						reg_file[5'h00][i] = 1'b0;
					end
				end

				else if(32 >= char_len > 0) begin // 32-1 Characters
					for(int i=char_len-1; i >= 0; i--) begin
						exp_tx.queue.push_back(reg_file[5'h00][i]);
						reg_file[5'h00][i] = 1'b0;
					end
					results_ap.write(exp_tx);
				end

				//$display("exp_tx: %s", exp_tx.convert2string());
				//uvm_report_info(get_name(), "predictor sends exp_tx out");

				// TODO: Account for GO_BSY set BEFORE ANY OTHER WRITES TO ANY OTHER EG (random stimulus from the start)
				// TODO: Account for G0_BSY writes during a transmission (should do nothing)
			end
			// TODO: READ
		end

	endfunction: write
endclass: predictor

class comparator extends uvm_component;

	`uvm_component_utils(comparator)

	uvm_analysis_export#(spi_transaction) axp_in;
	uvm_analysis_export#(spi_transaction) axp_out;
	uvm_tlm_analysis_fifo#(spi_transaction) expfifo;
	uvm_tlm_analysis_fifo#(spi_transaction) outfifo;

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		axp_in = new("axp_in", this);
		axp_out = new("axp_out", this);
		expfifo = new("expfifo", this);
		outfifo = new("outfifo", this);
	endfunction: build_phase

	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);

		axp_in.connect(expfifo.analysis_export);
		axp_out.connect(outfifo.analysis_export);
	endfunction: connect_phase

	task run_phase(uvm_phase phase);
		spi_transaction exp_tx, out_tx;

		forever begin
			uvm_report_info(get_name(), ": entered comparator forever loop");
			expfifo.get(exp_tx);
			uvm_report_info(get_name(), ": expfifo got exp_tx, waiting for outfifo to get out_tx");
			outfifo.get(out_tx);
			uvm_report_info(get_name(), ": outfifo got out_tx");

			if(out_tx.queue == exp_tx.queue) begin
				// Test Passed
				uvm_report_info(get_name(), ": Test Passed");
				//exp_tx.print();
				//out_tx.print();
				//`uvm_info("Test Passed ", $sformatf("\nActual=%s \nExpected=%s \n", out_tx.convert2string(), exp_tx.convert2string()), UVM_HIGH)

				/*
				while(0 != out_tx.queue.size()) begin
					out_tx.queue.pop_back();
				end
				while(0 != exp_tx.queue.size()) begin
					exp_tx.queue.pop_back();
				end
				*/
			end
			else begin
				// Test Failed
				//`uvm_info("Test Passed ", $sformatf("\nActual=%s \nExpected=%s \n", out_tx.convert2string(), exp_tx.convert2string()), UVM_HIGH)
				uvm_report_info(get_name(), ": Test Failed");
				////`uvm_info("Test Failed", { "\n", exp_tx.sprint(), "\n", out_tx.sprint()}, UVM_LOW)
				//exp_tx.print();
				//out_tx.print();
			end

			$display("out_tx: %s", out_tx.convert2string());
			$display("exp_tx: %s", exp_tx.convert2string());
		end

	endtask: run_phase
endclass: comparator

class apb_scoreboard extends uvm_scoreboard;

	`uvm_component_utils(apb_scoreboard)

	uvm_analysis_export#(apb_transaction) axp_in;
	uvm_analysis_export#(spi_transaction) axp_out;
	predictor m_predictor;
	comparator m_comparator;

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		axp_in = new("axp_in", this);
		axp_out = new("axp_out", this);
		m_predictor = predictor::type_id::create("m_predictor", this);
		m_comparator = comparator::type_id::create("m_comparator", this);
	endfunction: build_phase

	function void connect_phase(uvm_phase phase);
		axp_in.connect(m_predictor.analysis_export);
		axp_out.connect(m_comparator.axp_out);

		m_predictor.results_ap.connect(m_comparator.axp_in);
	endfunction: connect_phase

endclass: apb_scoreboard
`endif
