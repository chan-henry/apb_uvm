`ifndef SPI_DRIVER_SVH
`define SPI_DRIVER_SVH
class spi_driver extends uvm_driver#(spi_transaction);
	
	`uvm_component_utils(spi_driver)
	
	virtual spi_if my_spi_if;
	
	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new
	
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		
		if(!uvm_config_db#(virtual spi_if)::get(.cntxt(this),.inst_name(""),.field_name("spi_if"),.value(my_spi_if))) begin
      			`uvm_fatal(get_name(), "Failed to get virtual interface for driver")
		end
	endfunction: build_phase
	
	task run_phase(uvm_phase phase);
		spi_transaction tx;
		
		/*/////////////////////////////////////////////
			  _   _   _   _   _
		SCLK	_| |_| |_| |_| |_| |_
			__
		SS	  \__________________
			__ __________________
		MOSI	__X___X___X___X___X__
			__ __________________
		MISO	__X___X___X___X___X__
		
		////////////////////////////////////////////*/

		forever begin: driver_run_phase
			seq_item_port.try_next_item(tx);
			phase.raise_objection(.obj(this), .description(get_name()));

			while( null == tx ) begin: idle
				//uvm_report_info(get_name(), ": entered spi_driver run_phase idle loop");
				@my_spi_if.sclk;
				my_spi_if.miso	<= 1'b0;
			end: idle
		/*	
			while(my_spi_if.ss == 1) begin: wait_for_ss
				uvm_report_info(get_name(), ": looping in spi_driver");
				@(my_spi_if.ss);
			end: wait_for_ss
		*/	
			// Currently this will always respond with a loopback
		/*
			while(0 != tx.queue.size()) begin: transmit_miso
				uvm_report_info(get_name(), ": entered transmit_miso driver loop");
				my_spi_if.miso <= tx.queue.pop_front();
				@my_spi_if.cb;
			end: transmit_miso
		*/
			seq_item_port.item_done();
			phase.drop_objection(.obj(this), .description(get_name()));
		end: driver_run_phase

	endtask: run_phase
	
endclass: spi_driver
`endif
